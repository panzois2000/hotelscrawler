var request = require('request');
var cheerio = require('cheerio');
const readline = require("readline");
const Tripadvisor_Hotels = require('./models/users');

// ======================================================================================================

var mainURL = "https://www.tripadvisor.com.gr/Hotels-g189417-Heraklion_Crete-Hotels.html";

var hotelURLs=[]; //Array which contains the URLs that display the list of the hotels

var hotelsArray=[]; //Array that contains the names of the hotels and the review URL of the first page 

// =======================================================================================================
const rl = readline.createInterface({ 
	input: process.stdin,
	output: process.stdout
});
// ======================================================================================================= 
function doRequest(url) {
    return new Promise(function (resolve, reject) {
      request(url, function (error, res, body) {
        if (!error && res.statusCode == 200) {  
            resolve(body);
        } else {
            reject(error);
        }
      });
    });
  }
//========================================================================================================
async function visitPage(url) {
    console.log("Visiting page " + url);
    
    let body  = await doRequest(url);	
	// Parse the document body
    var $ = cheerio.load(body);
    getHotelUrls($);
    
    const array = hotelURLs.map((url) => (doRequest(url)));
    const results = await Promise.all(array);
    
    results.forEach(function(item,index){
        
        console.log("Visiting page "+ hotelURLs[index]);
        // Parse the document body
        var $ = cheerio.load(item);
        getHotelNames($); 

    });

    const promises = hotelsArray.map( (object)=>( doRequest(object.reviewUrl)));
    
    console.log("Loading the hotel titles...");
    
    const bodies = await Promise.all(promises); 
    getReviewUrls(bodies);
    
    console.log("Loading the hotel reviews...");
    
    var reviewPromises;
    for(var i=0; i< hotelsArray.length; i++){
        
        reviewPromises=[];
        reviewPromises.length = hotelsArray[i].manyReviewUrls.length;
        reviewPromises = hotelsArray[i].manyReviewUrls.map( (url)=>( doRequest(url)));
        const reviewBodies = await Promise.all(reviewPromises);
        findReviewBodies(reviewBodies,i);
        storeToDB(hotelsArray[i].title,hotelsArray[i].reviews,hotelsArray[i].evaluations);
        console.log(hotelsArray[i].title + "'s store completed!");
        // console.log(hotelsArray[i].title);
        // console.log(hotelsArray[i].evaluations.length);
        // console.log(hotelsArray[i].reviews.length);
    }

    console.log("Thank you!!");
}


// ========================================================================================================
// Stores the urls of the hotels in hotelsURL array
function getHotelUrls($){
    var count=0; 
    // Parse the document body
    var lastPageNum=$(' .last ');
    var flag = 0;
    lastPageNum.each( function(index) {
        lastPageNum=$(this).text();
        flag=1;
    });
    
    if(flag===0){
        lastPageNum = "1";
    }
    hotelURLs[0] = mainURL;
    var splitArray=mainURL.split('-');
    for(var i=1; i< lastPageNum; i++){   
        count+=30;
        hotelURLs[i] = splitArray[0] + "-" + splitArray[1] +"-" + "oa" + count + "-" + splitArray[2] + "-"+ splitArray[3];
    }
}
// =======================================================================================================
function getHotelNames($) {
    
    //--------HOTEL NAMES AND REVIEW URLS--------------
    const reviews=[];
    const manyReviewUrls=[];
    const evaluations=[];
    const tag=$('.listItem');
    tag.each(function( index ) {
        if($(this).find('.review_count').attr('href')!= undefined){
            const title=$(this).find('.property_title').text();
                const reviewUrl="http://www.tripadvisor.com.gr" + $(this).find('.review_count').attr('href');
                hotelsArray.push({ title, reviewUrl, manyReviewUrls, evaluations, reviews });
        }   
    });
    filterUnique();
    
}
// =======================================================================================================
function filterUnique(){
    var uniqueArray=[];
    uniqueArray = hotelsArray.filter(function(item, pos) {
    return hotelsArray.indexOf(item) == pos;
    });
    hotelsArray=uniqueArray;
}
// =======================================================================================================
function getReviewUrls(Array){
    Array.forEach(function(body,index){
        var count = 0;
        var $ = cheerio.load(body);
        var lastPageNum=$(' .pageNum ');
        var flag = 0;
        lastPageNum.each( function(index) {
            lastPageNum=$(this).text();
            flag=1;
        });

        if(flag===0){
            lastPageNum = "1";
        }
        var splitArray=hotelsArray[index].reviewUrl.split('-');
        hotelsArray[index].manyReviewUrls = [] ;
        hotelsArray[index].manyReviewUrls.push(hotelsArray[index].reviewUrl);
        for(var j=1; j<lastPageNum; j++){   
            count+=5;
            hotelsArray[index].manyReviewUrls.push(splitArray[0] + "-" + splitArray[1] +"-" + splitArray[2] + "-"+ splitArray[3] + "-" + "or" + count + "-" + splitArray[4] + splitArray[5]);
        }
    });
}
// =======================================================================================
function findReviewBodies(bodies,i){
    bodies.forEach(function(body){
        var $ = cheerio.load(body);
        // for stars of review
        var stars=$(' .nf9vGX55   ');
        stars.each(function(){
            // text of review
            var starNum;
            if($(this).find(' .bubble_50 ').length!==0){
                starNum=5;
            }else if($(this).find(' .bubble_40 ').length!==0){
                starNum=4;
            }else if($(this).find(' .bubble_30 ').length!==0){
                starNum=3;
            }else if($(this).find(' .bubble_20 ').length!==0){
                starNum=2;
            }else if($(this).find(' .bubble_10 ')!==0){
                starNum=1;
            }
            hotelsArray[i].evaluations.push(starNum);
        });
        // for boxes of review
        var review=$(' ._2wrUUKlw  ');
        review.each(function(){
            // text of review
            review=$(this).find(' .IRsGHoPm ').text();
            hotelsArray[i].reviews.push(review);
        });
    });
}
// =======================================================================================
function storeToDB(tit,rev,eval){
    Tripadvisor_Hotels.create({
        name: tit,
        reviews: rev,
        evaluations: eval,
    }).then((resp)=>{
        console.log(tit);
    }).catch((err)=>{
        console.log(err);
    });
    
}
// =======================================================================================
visitPage(mainURL);